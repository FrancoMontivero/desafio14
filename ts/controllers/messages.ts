import fs from 'fs';
import path from 'path';
import { emitMessages } from './socket';

export interface Message {
    from: string,
    message: string,
    hour: string
}

export function getMessages(): {[key: string]: Message[]} {
    try {
        const data: string = fs.readFileSync(path.resolve(__dirname, '../messages.json'), {encoding: 'utf-8'});
        const messages: {any: Message[]} = JSON.parse(data);
        return messages;
    } catch (err) {
        return {} 
    }
}

export async function addMessage(fullDate: Date, message: string, from: string): Promise<{[key: string]: Message} | null> {
    try {
        const newMessage: Message = {
            from, 
            message, 
            hour: `${fullDate.getHours() < 10 ? `0${fullDate.getHours()}` : fullDate.getHours()}:${fullDate.getMinutes() < 10 ? `0${fullDate.getMinutes()}` : fullDate.getMinutes()}`
        };
        const messages = await getMessages();
        const date: string = fullDate.toString().split(' ').slice(0, 4).join(" ");
        if(messages.hasOwnProperty(date)) messages[date].push(newMessage);
        else messages[date] = [newMessage];       
        fs.writeFileSync(path.resolve(__dirname, '../messages.json'), JSON.stringify(messages));
        return {[date]: newMessage};
    } catch (err) {
        return null;
    }
}