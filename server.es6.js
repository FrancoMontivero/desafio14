import http from 'http';
import express from 'express';
import path from 'path';
import { Server, Socket } from "socket.io";

import cors from './src/middlewares/cors';
import multer from './src/middlewares/multer';
import morgan from './src/middlewares/morgan';
import errorHandler from './src/middlewares/errorHandler';

import { initServer } from './src/controllers/socket';

import { SHOW_REQUEST } from './src/config';

import router from './src/routes';

const app = express();
const server = http.createServer(app);
const io = new Server(server);
initServer(io);

app.use(express.json());
app.use(multer.array('any'));
app.use(express.urlencoded({ extended : true }));
app.use('/static', express.static(path.resolve(__dirname, 'assets')));

if(SHOW_REQUEST === 'enabled') app.use(morgan); 

app.use(cors);

app.use(router);
app.use(errorHandler);

export default server;
