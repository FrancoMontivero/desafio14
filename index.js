const server = require('./server')['default'];
const { PORT } = require('./src/config');

server.listen(PORT, () => {
    console.log(`listen in the port ${PORT}`);
})
