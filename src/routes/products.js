"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const products_1 = require("../controllers/products");
const routerProducts = express_1.default.Router();
routerProducts.get('/listar', (req, res) => {
    try {
        if (products_1.getProducts().length === 0)
            res.json({ error: "No hay productos cargados" });
        else
            res.json(products_1.getProducts());
    }
    catch (err) {
        res.json({ "error": err.message });
    }
    ;
});
routerProducts.get('/listar/:id', (req, res) => {
    try {
        res.json(products_1.getProduct(parseInt(req.params.id)));
    }
    catch (err) {
        res.json({ "error": err.message });
    }
    ;
});
routerProducts.post('/guardar', (req, res) => {
    const { title, price, thumbnail } = req.body;
    try {
        res.json(products_1.addProduct(title, price, thumbnail));
    }
    catch (err) {
        res.json({ "error": err.message });
    }
    ;
});
routerProducts.put('/actualizar/:id', (req, res) => {
    const { title, price, thumbnail } = req.body;
    const id = parseInt(req.params.id);
    try {
        res.json(products_1.updateProduct(id, title, price, thumbnail));
    }
    catch (err) {
        res.json({ "error": err.message });
    }
    ;
});
routerProducts.delete('/borrar/:id', (req, res) => {
    const id = parseInt(req.params.id);
    try {
        res.json(products_1.removeProduct(id));
    }
    catch (err) {
        res.json({ "error": err.message });
    }
});
exports.default = routerProducts;
