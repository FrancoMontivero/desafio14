"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addMessage = exports.getMessages = void 0;
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
function getMessages() {
    try {
        const data = fs_1.default.readFileSync(path_1.default.resolve(__dirname, '../messages.json'), { encoding: 'utf-8' });
        const messages = JSON.parse(data);
        return messages;
    }
    catch (err) {
        return {};
    }
}
exports.getMessages = getMessages;
function addMessage(fullDate, message, from) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const newMessage = {
                from,
                message,
                hour: `${fullDate.getHours() < 10 ? `0${fullDate.getHours()}` : fullDate.getHours()}:${fullDate.getMinutes() < 10 ? `0${fullDate.getMinutes()}` : fullDate.getMinutes()}`
            };
            const messages = yield getMessages();
            const date = fullDate.toString().split(' ').slice(0, 4).join(" ");
            if (messages.hasOwnProperty(date))
                messages[date].push(newMessage);
            else
                messages[date] = [newMessage];
            fs_1.default.writeFileSync(path_1.default.resolve(__dirname, '../messages.json'), JSON.stringify(messages));
            return { [date]: newMessage };
        }
        catch (err) {
            return null;
        }
    });
}
exports.addMessage = addMessage;
