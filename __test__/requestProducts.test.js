const faker = require('faker');
const supertest = require('supertest');
const app = require('../server.js')['default'];
 
const PRFEIX_FILE_STATICS = '/static';

const PREFIX = '/api/productos'
const PUT_PRODUCT = PREFIX + '/actualizar';
const POST_PRODUCT = PREFIX + '/guardar';
const GET_PRODUCTS = PREFIX + '/listar';
const DELETE_PRODUCT = PREFIX + '/borrar';

faker.locale = 'es';

describe("request products", () => {
	const agent = supertest(app);
	let products = [];
	for(let i = 0 ; i < 10 ; i++) {
		products.push({
			id: i + 1,
			title: faker.commerce.product(),
			price: parseFloat(faker.commerce.price()),
			thumbnail: faker.image.imageUrl()
		});
	}
	
    it("GET /api/productos/listar (empty products)", async () => {
        let response = await agent.get(GET_PRODUCTS);
		expect(response.status).toBe(200);
		expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
		expect(response.body instanceof Object).toBeTruthy();
		expect(response.body.hasOwnProperty('error')).toBeTruthy();
		expect(response.body).toEqual({error: "No hay productos cargados"});
    })

	it('the response for the file static products.hmtl should correct', async () => {
		const response = await agent.get( `${PRFEIX_FILE_STATICS}/products.html`);
		expect(response.status).toBe(200);
		expect(response.headers['content-type']).toBe('text/html; charset=UTF-8');
		expect(response.text).not.toBe('');
	})

	it('the response for the file static chat.hmtl should correct', async () => {
		const response = await agent.get( `${PRFEIX_FILE_STATICS}/chat.html`);
		expect(response.status).toBe(200);
		expect(response.headers['content-type']).toBe('text/html; charset=UTF-8');
		expect(response.text).not.toBe('');
	})

    describe('POST /api/productos/guardar', () => {
        it("the response should be a error when missing parameters", async () => {
            const response = await agent
                .post(POST_PRODUCT)
                .send({title: faker.commerce.product(), price: faker.commerce.price()})
            expect(response.status).toBe(200);
            expect(response.body instanceof Object).toBeTruthy();
            expect(response.body.hasOwnProperty('error')).toBeTruthy()
            expect(response.body).toEqual({error: "Hay parametros vacios o indefinidos"});
            
        });

        it("the response should be a error when price has at least one non-numeric character", async () => {
            const response = await agent
                .post(POST_PRODUCT)
                .send({title: faker.commerce.product(), price: "20A1", thumbnail: faker.image.imageUrl()})

            expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
            expect(response.body instanceof Object).toBeTruthy();
            expect(response.body.hasOwnProperty("error")).toBeTruthy();
            expect(response.body).toEqual({error: "El precio no puede contener caracteres no numericos"});
        })

        it("the response should be the product loaded when parameters aren't missing", async () => {
            const response = await agent
                .post(POST_PRODUCT)
                .send(products[0]);

            expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
            expect(response.body instanceof Object).toBeTruthy();
            expect(response.body).toEqual(products[0]);
        });

        it("should to assign the proper id", async () => {	
			for(let product of products.slice(1)) {
				await agent.post(POST_PRODUCT).send(product);
			}

            const response = await agent.get(GET_PRODUCTS);
            expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
            expect(response.body instanceof Array).toBeTruthy();
            expect(response.body).toEqual(products);
        });
    });
    
	describe('DELETE /api/productos/eliminar/:id', () => {
		const id = 10;

		it("the response should be a error when the id is non-numeric character", async () => {
			const response = await agent.delete(`${DELETE_PRODUCT}/a`);
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeTruthy();
			expect(response.body).toEqual({"error": "El id del producto debe ser un caracter numerico"})
		})

		it("the response should be a error when the hasn't none product with that id", async () => {
			const response = await agent.delete(`${DELETE_PRODUCT}/100`);
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeTruthy();
			expect(response.body).toEqual({"error": "No existe un producto con el id 100"})
		})

		it("the response should be the product removed", async () => {
			const response = await agent.delete(`${DELETE_PRODUCT}/${id}`);
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeFalsy();
			expect(response.body).toEqual(products[id - 1]);
		})

		it("the listing should not has the product removed", async () => {
			const response = await agent.get(`${GET_PRODUCTS}`);
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Array).toBeTruthy();
			expect(response.body.find(e => e.id === id)).toBeUndefined();
		})
	})

	describe(`PUT ${PUT_PRODUCT}/:id`, () => {
		const id = 8;
		const newData = {
			title: faker.commerce.product(),
			price: parseFloat(faker.commerce.price()),
			thumbnail: faker.image.imageUrl()
		};

		it("the response should be a error when parameters are missing", async () => {
			const response = await agent
				.put(`${PUT_PRODUCT}/${id}`)
				.send({title: faker.commerce.product(), price: faker.commerce.price()});
			expect(response.status).toBe(200);
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeTruthy();
            expect(response.body).toEqual({error: "Hay parametros vacios o indefinidos"});
        })

		it('the response should be a error when the id is non-numeric character', async () => {
			const response = await agent
				.put(`${PUT_PRODUCT}/a`)
				.send(newData);
			expect(response.status).toBe(200);
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeTruthy();
			expect(response.body).toEqual({"error": "El id del producto debe ser un caracter numerico"});
		})
	
        it("the response should be a error when the price has a non-numeric character", async () => {
			const response = await agent
				.put(`${PUT_PRODUCT}/${id}`)
				.send({title: faker.commerce.product(), price: faker.lorem.word(), thumbnail: faker.image.imageUrl()});
			expect(response.status).toBe(200);
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeTruthy();
            expect(response.body).toEqual({error: "El precio no puede contener caracteres no numericos"});
        })

		it("the response should be a error when hasn't none product with that id", async () => {
			const response = await agent.put(`${PUT_PRODUCT}/100`).send(newData);
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeTruthy();
			expect(response.body).toEqual({"error": "No existe un producto con el id " + 100})
		})

		it('the response should be the updated product', async () => {
			const response = await agent.put(`${PUT_PRODUCT}/${id}`).send(newData)
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeFalsy();
			expect(response.body).toEqual({id, ...newData});
		})

		it('the update product should appear in the listing', async () => {
			const response = await agent.get(`${GET_PRODUCTS}`);
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Array).toBeTruthy();
			expect(response.body.find(e => e.id === id)).not.toBeUndefined();
			expect(response.body.find(e => e.id === id)).toEqual({id, ...newData})

		})

		it('the update product should appear proper', async () => {
			const response = await agent.get(`${GET_PRODUCTS}/${id}`);
			expect(response.status).toBe(200);
			expect(response.headers['content-type']).toBe('application/json; charset=utf-8');
			expect(response.body instanceof Object).toBeTruthy();
			expect(response.body.hasOwnProperty("error")).toBeFalsy();
			expect(response.body).toEqual({id: id, ...newData});
		})
	})
})
